document.addEventListener("DOMContentLoaded", function () {
    const fetchButton = document.getElementById("fetchButton");

    fetchButton.addEventListener("click", async () => {
        try {
            const currentOrigin = window.location.origin;
            const targetPath = '/.netlify/functions/api/gate';
            const apiUrl = currentOrigin + targetPath;
            const response = await fetch(apiUrl)
            const status = await response.json()
            console.log(status.success)
            if (status.success) {
                fetchButton.style.transitionDuration = "200ms"
                fetchButton.style.backgroundColor = "#62fc03"
                setTimeout(() => { fetchButton.style.backgroundColor = "#03b1fc" }, 1500)
            } else {
                console.log("error")
            }
        } catch (error) {
            console.log("error")
        }
    });
});