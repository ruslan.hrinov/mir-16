const openGate = async (req, res) => {

  const url = 'https://mir16.home.jelly.cloud:3000/graphql';
  const operationName = 'doorOpen';
  const variables = { doorType: 'gate' };
  const query = `
  query doorOpen($doorType: String!) {
    doorOpen(doorType: $doorType) {
      success
      error {
        Type
        Text
        waitSeconds
        __typename
      }
      __typename
    }
  }
`;

  const requestOptions = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      "Authorization": "IMC9"
    },
    body: JSON.stringify({
      operationName,
      variables,
      query,
    }),
  };
  await fetch(url, requestOptions)
    .then(response => response.json())
    .then(data => {
      return res.status(200).json({ success: data?.data?.doorOpen?.success });
    })
    .catch(error => {
      console.log(error)
      return res.status(500).json({ error: "Internal Server Error" });
    });
}

module.exports = {
  openGate
}