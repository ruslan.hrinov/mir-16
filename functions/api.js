
import express, { Router } from "express";
const gateController = require("../controllers/gateController.js");
import serverless from "serverless-http";
const cors = require('cors');
const api = express();
const router = Router();
router
    .route("/gate")
    .get(gateController.openGate)
api.use('/.netlify/functions/api', router);
api.use(cors)

export const handler = serverless(api);